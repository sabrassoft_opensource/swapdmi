==Compare/Contrast with Active Record==

First and foremost, Swap-DMI is a *toolkit for building* a data-access layer to an application,
vs. a fully functional out-of-the-box layer like Active Record. 
Swap-DMI users are *expected* to need to write some code to accomplish what they want,
while Active Record, like the majority of Ruby on Rails, adopts a convention over configuration approach
which actively seeks to avoid users having to write any code at all.  
  
The advantage in being a toolkit vs. an out-of-the-box ready-to-use solution is Swap-DMI can
much more easily accomodate unusual or unique requirements... Since it doesn't have to support
an assumed 'base case' for how things should function, it imposes few restrictions on what you can do and how you can do it.  
  
The second most important difference, related to the first, is that Active Record makes a strong assumption that
the thing you will be talking to is a single RDBMS database, which is moreover formatted and setup in a very specific way.  

There are drivers for Active Record that do things like let it do things like connect to some REST services, but the strong 
assumptions that Active Record requires on how data is organized, accessed, and named means that one size simply can't fit all.

If you need to connect to a type of data source for which a driver does not already exist then you will need to write your own,
which means doing the same work you would for Swap-DMI, but in a framework that isn't as well-documented or as easy-to-use, 
because it was built with the expectation that most users would not need to do that.  

Swap-DMI, on the other hand, is designed from the ground up to handle any type of data source; and because it assumes
that all users will be writing integration code, it tries to make that process as easy & well-documented as possible 

The final difference is on what design patterns, Active Record gets it's name from the *Active Record* design pattern,
which is best summed up by the assertion that the data model objects themselves should be in charge of all behavior
related to the cogent data.  
 
As an example, to find the user named Fred and change his name to Mark, we 1st make a call to the class method find_by of
the Users class, searching by name == Fred, and finding the appropriate instance of User. We then re-populate the name field, and call
the save instance method to re-propogate the database 
 
user = Users.find_by(:name => 'Fred')
user.name = 'Mark'
user.save!

In contrast to the Active Record pattern, there is the Data Access Object (D.A.O => Dao) pattern. With the Dao pattern updates to the data source are performed not by the data model object, but instead a dedicated actor component.  
  
user = userDao.find_by(:name => 'Fred')
user.name = 'Mark'
userDao.save(user) 

The advantage with the Dao pattern is that it greatly simplifies the implementation of the data model objects, in the Active Record pattern each instance of a data model needs to have behavior to work with & sustain a connection to a data source, 
and in addition we have to have class level behavior to create & retrieve the instances in the first place. Data source connections are also often expensive to create & persist... further complicating things by requiring the data models to work with pooling & shared resources across all instances.  

Swap-DMI leans towards a hybrid approach, DataSources are used to create & retrieve model instances, but model instances themselves handle updating their associated records. It is also possible to operate in a pure Dao or pure Active Record model... 
You can simply not create methods on your Model classes to handle updating associated data and instead put that logic in the DataSource
to achieve a pure Dao setup, and vice versa you can extend the Model class with class level methods to act as a DataSource to operate in a pure Active Record way. 